# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 13:57:11 2016
@author: Yampy


v2.0 changes
- modded datetime calls to one call only (resulting in ONE timestamp as opposed to like 234659872354x ones called within milliseconds of one another
- Passed study and timestamp to camera test (in order to)
    - removed '()' from foldernames
- verified that LED_Voltage is set to 1.2V (700mA for a 0.7 A board)
- fixed Uniformity image capture (FPN.bmp writing over Uniformity_camera.bmp)
- made a reduced csv that contains pertinent info from output.log into output.csv
- reduced results file to show only one test
- removed all space and capitals and partentheces

v2.0.1
- added test printout
- fixed 2.0 bugs
- added zip
- added manifest
- added hooks for s3 upload

v2.0.2
- automated Power Measurement via PyVisa
- fixed typos
- Updated output to include Pass/Fail on screen upon completion
- Running Uniformity Test code for the first time
- updated power measurement position
- updated text output for final result

v2.0.3
- deactivated uniformity measurement
- added a pause variable to pause the program at each point an image is collected
- Moved a pause statement to not pause unitl after the pedestal step is completed
- Updated the Serial Number Input (8 digits)
- updated scope on
- updated Scope LED on time
- Updated typos to reflect updated serial number input
- Added Uniformity Analysis
- Added pass criteria of camera uniformity roll off and LED Uniformity roll off
- Updated with uniformity analysis and updated pass/fail criteria
- Added stuff to fix zipping/upload
- Fixed Input serial bug
- Fixed Zipping file not having full folder tree
- Modified MTF 5 and MTF50 to take MIN not MEAN of all region's data
- Power Measurement Automated
- Uniformity Analysis Automated

v2.0.4
- updated  Pass/Fail to read correct column for new data frame structure
- Changed point Pass/Fail is run to display result at the bottom of the output
- Added Toshiba Serial Number
- Corrected pass criteria for DOV to 5 degrees
- Added operator name input
- changed file output name order
- fixed typos
- implemented cloud upload

v3.0.0
- Update Pass/Fail line output message
- Increased MTF pass limit to 10 ln/mm based on 
- reinstated Lot number input format ######-##
- implemented try/except structures
- added natural angle spec to pass fail
- edited comments
- changed output font color for results
- display errors and failure parameters if scope fails
- color result output

v4.0.0
- implemented new FOV and Pointing algorithms
- alowed graceful crash for colorbalance and alighnment process failures

"""
version = '4.0.0'

import time
import numpy as np
import pandas as pd
import sys
import os
from Acquisition.Logger import Logger
from time import localtime, strftime
from Acquisition.CameraTestClass import CameraTest
from Analysis.Analysis import CameraAnalysis
from Acquisition.ZaberStageClass import ZaberStage
from admin import admin
from colorama import init, Back, Style
init()



def getDateTime():
    return str(strftime("%Y-%m-%d_%H-%M-%S", localtime()))


# Start
print("\n##############################\n# Camera Test Station v" + version + " #\n##############################\n")

############## Scope Entry ##########################

# get study name

operator = input("Please input the operator's name (First initial, last name): ")

serialnumber = input("Please input the scope's serial number (SSSSSSSS Portion): ")
try:
    seriallen = len(serialnumber)
    serialnumber = int(serialnumber)
except:
    pass

while not type(serialnumber) == int or not seriallen == 8:
    serialnumber = input('Serial number incorrectly entered, please enter serial number in the correct format (SSSSSSSS): ')
    try:
        seriallen = len(serialnumber)
        serialnumber = int(serialnumber)
    except:
        pass
  
camera_serialnumber = input("Please input the scope's Toshiba camera serial number (SSSSSSSS Portion): ")
try:
    camera_seriallen = len(camera_serialnumber)
    camera_serialnumber = int(camera_serialnumber)
except:
    pass

while not type(camera_serialnumber) == int or not camera_seriallen == 8:
    camera_serialnumber = input('Toshiba camera serial number incorrectly entered, please enter serial number in the correct format (SSSSSSSS): ')
    try:
        camera_seriallen = len(camera_serialnumber)
        camera_serialnumber = int(camera_serialnumber)
    except:
        pass

batchbatch = input("Please input the scope's batch number: ")
while True:
    try:
        test_lot_1 = int(batchbatch)
        break
    except:
        lotlot = input("Lot incorrectly entered, please input the scope's lot number: ")

while True:
    if len(batchbatch) > 10:
        batchbatch = input("Lot incorrectly entered, please input the Scope's scope's lot number: ")
        break
    else:
        break
    
digits = len(batchbatch)
batchbatch = "0"*(10-digits) + batchbatch
    


# study = 'Toshiba_SN_' + input("Please input the Camera ID number (AS IT APPEARS!!!! i.e. XXXXXX-XX-XX): ")
study = 'scope_sn_' + str(serialnumber)  # +'-'+str(lotlot)+'-'+str(numbernumber)
camera_SN = 'toshiba_sn_' + str(camera_serialnumber)
notes = input("Any Notes?: ")
input('\nLoad the scope into its natural position, as described in the MPI, press enter when complete...')
######################Logging########################

# If study exists, change cwd into study, else, create and change
path = os.getcwd()
path_to_log = os.getcwd()
datetime = getDateTime()

sys.stdout = Logger(sys.stdout, path + '\\output.log')
sys.stderr = Logger(sys.stderr, path + '\\errors.log')

sys.stdout.write('\n\n' + datetime + ' Test: ' + str(study), True)
sys.stderr.write('\n\n' + datetime + ' Test: ' + str(study), True)

if not os.path.exists(path + "\\Data\\" + study):
    os.makedirs(path + "\\Data\\" + study)
    os.makedirs(path + "\\Data\\" + study + "\\" + datetime)
else:
    os.makedirs(path + "\\Data\\" + study + "\\" + datetime)

path = path + "\\Data\\" + study + "\\" + datetime

sys.stdout = Logger(sys.stdout, path + '\\output.log')
sys.stdout.write('Software Version v' + version, init=True)
sys.stderr = Logger(sys.stderr, path + '\\output.log')

with open(os.path.join(path, "Notes.txt"), "w") as text_file:
    print("Notes: {}".format(notes), file=text_file)
    
# Init Scope Stage 
scopeStage = ZaberStage('COM7', 1510000, 0)  # 1331629 is 0 mm from slanted edge
scopeStage.reset()

# init Target Stage
targetStage = ZaberStage('COM4', 3000000, 0)
targetStage.reset()

############### TEst #########################

# Start Test
test = CameraTest(path, study, datetime, camera_SN, batchbatch, operator, scopeStage, targetStage)

start = time.time()

#create acquistion completetion variables
#Print inputs for output log file
print('Logging OFS run by operator: ' +operator)
print('Auris Serial Number: '+ str(serialnumber))
print('Toshiba Serial Number: '+ str(camera_serialnumber))
print('Auris Batch Number: '+batchbatch)

white_balance = False
uniformity = False
farField = False
get_mtf_data = False
DNR = False
Distortion = False
veilingGlare = False
Clocking = False
colorTest = False
measPower = False
saveData = False
writeDataToJointBoard = False
cloud_upload = False


try:
    test.mtf_align()
    mtf_align = True
except Exception as e:
    print(str(e))
    mtf_align = False
    print(Back.YELLOW +'MTF alignment failed')
    input('Reset the OFS, there was an issue with the algorithm - Press Enter to quit')
    print(Style.RESET_ALL)    
    scopeStage.reset()
    sys.exit()
try: 
    test.white_balance()
    white_balance = True
except Exception as e:
    print(str(e))
    white_balance = False
    print(Back.YELLOW +'White balance connection broken')
    input('Reset the OFS, - Press Enter to quit')
    print(Style.RESET_ALL)  
    scopeStage.reset()   
    targetStage.reset()
    sys.exit()

try:
    test.uniformity()
    uniformity = True
except Exception as e:
    print(str(e))
    uniformity = False
    print('Uniformity collection failed')
try:
    test.farField()
    farField = True
except Exception as e:
    print(str(e))
    farField = False
    print('Far Field collection failed')
    
try:
    test.get_mtf_data()
    get_mtf_data = True
except Exception as e:
    print(str(e))
    get_mtf_data = False
    print('MTF data collection failed')
try:
    test.DNR()
    DNR = True
except Exception as e:
    print(str(e))
    DNR = False
    print('Dynamic range collection failed')
try:
    test.Distortion()
    Distortion = True
except Exception as e:
    print(str(e))
    distortion = False
    print('Distortion target collection failed')
try:
    test.veilingGlare()
    veilingGlare = True
except Exception as e:
    print(str(e))
    veilingGlare = False
    print('Veiling glare collection failed')
try:
    test.Clocking()
    Clocking = True
except Exception as e:
    print(str(e))
    Clocking = False
    print('Clocking collection failed')
try:
    test.colorTest()
    colorTest = True
except Exception as e:
    print(str(e))
    colorTest = False
    print('Color test data collection failed')
try:
    test.measPower()
    measPower = True
except Exception as e:
    print(str(e))
    measPower = False
    print('Power measurement collection failed')
try:
    test.saveData()
    saveData = True
except Exception as e:
    print(str(e))
    saveData = False
    print('Save Data failed')
try:
    test.writeDataToJointBoard()
    writeDatatoJointBoard = True
    print('Joint Board writing successful')
except Exception as e:
    print(str(e))
    writeDatatoJointBoard = False
    print('Joint Board writing failed')

test.close()

print("\nALL TESTS COMPLETED!")

print("Test took " + str(np.round(time.time() - start, 2)) + " seconds to complete!\n")

print("Proceeding to Analysis...")


analysis = CameraAnalysis(path, study, datetime)

start2 = time.time()

if get_mtf_data == True:
    try:
        analysis.run_MTF()
        run_MTF = True
    except Exception as e:
        print(str(e))
        run_MTF = False
        print('MTF Analysis failed')
else:
    run_MTF = False 

if uniformity == True:
    try:
        analysis.run_FOV()
        run_FOV = True
    except Exception as e:
        print(str(e))
        run_FOV = False
        print('FOV Analysis failed')
else:
    run_FOV = False 

if get_mtf_data == True:
    try:
        analysis.run_Pointing()
        run_Pointing = True
    except Exception as e:
        print(str(e))
        run_Pointing = False
        print('Pointing Analysis Failed')
else:
    run_Pointing = False
    
if run_Pointing == True and run_FOV == True:
    run_FOV_Pointing = True
else:
    run_FOV_Pointing = False
    
if DNR == True:
    try:
        analysis.run_Dynamic_Range()
        run_Dynamic_Range = True
    except Exception as e:
        print(str(e))
        run_Dynamic_Range = False
        print('Dynamic range analysis failed')
else:
    run_Dynamic_Range = False
    
if uniformity  == True:
    try:
        analysis.run_Uniformity_analysis()
        run_Uniformity_analysis = True
    except Exception as e:
        print(str(e))
        run_Uniformity_analysis = False
        print('Uniformity analysis failed')
else:
    run_Uniformity_analysis = False

print("Analysis took " + str(np.round(time.time() - start2, 2)) + " seconds to complete!\n")

try:
    # Manifest, zip and send
    admin = admin(path, study, datetime)
    admin.manifest()
    admin.zip()
    admin.upload()
    admin.delete_30_day_old_data()
    cloud_upload = True
    print('Data Uploaded to the cloud succesfully')
except:
    cloud_upload = False


if run_Uniformity_analysis == True and run_FOV_Pointing == True and run_MTF == True and measPower == True and  white_balance == True:
    try: 
        print("\nALL ANALYSIS COMPLETED!")
        print("Running Pass/Fail Analysis")
        analysis.run_passfail()
    except Exception as e:
        print(str(e))
        print(Back.RED + 'The device FAILED, reject the unit!')
        print(Style.RESET_ALL)
        
else:
    data_path = os.path.join(path, study+'_'+datetime+'_'+'output.csv')
    
    print(Back.RED + 'The device FAILED, reject the unit!')
    print(Style.RESET_ALL)
    if white_balance == False:
        print('White Balance failed to complete')
    if uniformity == False:
        print('Uniformity data not collected')
    if farField == False:
        print('Farfield data not collected')
    if get_mtf_data == False:
        print('MTF data not collected')
    if DNR == False:
        print('Dynamic Range data not collected')
    if Distortion == False:
        print('Distortion data not collected')
    if veilingGlare == False:
        print('Veiling glare data not collected')
    if Clocking == False:
        print('Clocking data not collected')
    if colorTest == False:
        print('Color calibration data not collected')
    if measPower == False:
        print('Output power data not collected')
        entry = ['led_scope_power_mw']
        #data = [float('NaN')]
        data = [float('NaN')]
        dataFrame_add = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame = pd.read_csv(data_path)
        data_to_save = pd.concat([dataFrame, dataFrame_add], ignore_index=True)
        data_to_save.to_csv(os.path.join(path, study+'_'+datetime+'_'+'output.csv'), index=False,na_rep = "NULL")

    if saveData == False:
        print('Data save not completed')
    if writeDataToJointBoard == False:
        print('Joint board data failed to update')
    if run_Uniformity_analysis == False:
        print('Uniformity analysis failed')
        entry = ['camera_uniformity_roll_off','camera_uniformity_r_squared','LED_uniformity_roll_off','LED_uniformity_r_squared']
        #data = [float('NaN'),float('NaN'),float('NaN'),float('NaN')]
        data = [float('NaN'),float('NaN'),float('NaN'),float('NaN')]
        dataFrame_add = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame = pd.read_csv(data_path)
        data_to_save = pd.concat([dataFrame, dataFrame_add], ignore_index=True)
        data_to_save.to_csv(os.path.join(path, study+'_'+datetime+'_'+'output.csv'), index=False,na_rep = "NULL")

    if run_FOV_Pointing == False:
        print('FOV analysis failed')
        entry = ['field_of_view_degrees', 'pointing_deviation_degrees']
        #data = [float('NaN'),float('NaN')]
        data = [float('NaN'),float('NaN')]
        dataFrame_add = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame = pd.read_csv(data_path)
        data_to_save = pd.concat([dataFrame, dataFrame_add], ignore_index=True)
        data_to_save.to_csv(os.path.join(path, study+'_'+datetime+'_'+'output.csv'), index=False,na_rep = "NULL")

        
    if run_MTF == False:
        print('MTF analysis failed')
        entry = ['mtf-50_25.0_mm','mtf-50_50.0_mm','mtf-50_3.0_mm','mtf-50_7.0_mm',
                 'mtf_150_lp-mm_25.0_mm','mtf_150_lp-mm_50.0_mm','mtf_150_lp-mm_3.0_mm','mtf_150_lp-mm_7.0_mm']
        #data = [float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN')]
        data = [float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN'),float('NaN')]
        dataFrame_add = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame = pd.read_csv(data_path)
        data_to_save = pd.concat([dataFrame, dataFrame_add], ignore_index=True)
        data_to_save.to_csv(os.path.join(path, study+'_'+datetime+'_'+'output.csv'), index=False,na_rep = "NULL")

    if run_Dynamic_Range == False:
        print('DNR analysis failed')
        entry = ['max_dynamic_range_resolved']
        data = [float('NaN')]
        #data = ['FAIL']
        dataFrame_add = pd.DataFrame({'value': data, 'attribute': entry})
        dataFrame = pd.read_csv(data_path)
        data_to_save = pd.concat([dataFrame, dataFrame_add], ignore_index=True)
        data_to_save.to_csv(os.path.join(path, study+'_'+datetime+'_'+'output.csv'), index=False,na_rep = "NULL")
        
    
if cloud_upload == False:
    print('Cloud Upload Failed, Check Network Connections and Computer Time, repeat the measurement')
    
input("Press Enter to Proceed to Quit...")

sys.stderr.close_logger()
sys.stdout.close_logger()
