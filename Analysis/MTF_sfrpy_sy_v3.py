"""
    compute mtf on slanted-wedge image
    based on matlab code sfrmat3 originally written by Peter Burns 2009-2015
    the original code handles the three color channels of a color RGB image
    for this version, I have omitted this for grayscale input images only
"""
import numpy as np
import cv2
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def getEdge(img, verbose=False):
    # Turn Grayscale
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('Original')
        plt.imshow(img)
        plt.show()

    d_img = img.copy()
    edges = cv2.Canny(d_img, 50, 150, apertureSize = 3)

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('Edges')
        plt.imshow(edges)
        plt.show()

    # Search for line using Hough, Find EXACTLY 4 line sets
    j = 70
    num_lines = 4
    while True:

        lines = cv2.HoughLines(edges, 1, np.pi / 360, j, max_theta=np.pi)
        lines = lines.reshape(len(lines), 2)
        lines = lines[lines[:, 1].argsort()]

        angles = np.array(lines).T[1]
        split = []
        for i in range(len(angles) - 1):
            # If change is greater than ~30 deg
            if angles[i + 1] - angles[i] > 0.5:
                split.append(i + 1)

        if len(split) + 1 > num_lines:
            j += 10
        elif j == 0:
            break
        elif len(split) + 1 < num_lines:
            j -= 10
        else:
            break

    # get X,Y for lines found
    lines_pts = []
    for rho, theta in lines:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 1000 * (-b))
        y1 = int(y0 + 1000 * (a))
        x2 = int(x0 - 1000 * (-b))
        y2 = int(y0 - 1000 * (a))
        lines_pts.append([x1, y1, x2, y2])

    # Separate lines by split
    lines_list = []
    for i in range(num_lines):
        if i == 0:
            lines_list.append(lines_pts[0:split[0]])
        elif not i == num_lines - 1:
            lines_list.append(lines_pts[split[i - 1]:split[i]])
        else:
            lines_list.append(lines_pts[split[i - 1]:])

    # Separate angles by split
    angles_list = []
    for i in range(num_lines):
        if i == 0:
            angles_list.append(angles[0:split[0]])
        elif not i == num_lines - 1:
            angles_list.append(angles[split[i - 1]:split[i]])
        else:
            angles_list.append(angles[split[i - 1]:])

    # Consolidate all lines found to one line per split based on how many 255's it runs over
    angle_final = []
    line_final = []
    slope_final = []
    for i in range(num_lines):
        line_profile = []
        for j in lines_list[i]:
            j = np.array(j)
            line = createLineIterator(j[0:2], j[2:], edges)
            sums = np.sum(line)
            line_profile.append(sums)
        line_profile_max = np.argmax(line_profile)
        line_final.append(lines_list[i][line_profile_max])
        angle_final.append(angles_list[i][line_profile_max])
        x1 = lines_list[i][line_profile_max][0]
        y1 = lines_list[i][line_profile_max][1]
        x2 = lines_list[i][line_profile_max][2]
        y2 = lines_list[i][line_profile_max][3]
        if not x1 == x2:
            slope_final.append([(y1 - y2) / (x1 - x2), y1 - x1 * (y1 - y2) / (x1 - x2)])



    if verbose:
        print('Angles: ' + str(angle_final))
        print('Lines: ' + str(line_final))

    if verbose:
        for i in line_final:
            cv2.line(d_img, (i[0], i[1]), (i[2], i[3]), (255, 0, 0), 1)
        fig = plt.figure()
        fig.canvas.set_window_title('Hough')
        plt.imshow(d_img)
        plt.show()

    # Find Intersections
    sects = []
    for i in lines_list:
        for j in lines_list[lines_list.index(i) + 1:]:
            for k in i:
                for l in j:
                    k = np.array(k)
                    l = np.array(l)
                    sects.append(seg_intersect(k[0:2], k[2:], l[0:2], l[2:]))

    # Calculate Center from Intersects
    center = np.nanmean(sects, axis=0)
    if verbose:
        print('Center at: ' + str(center))

    # Cropping Calculations
    slope_rise = slope_final[0][1]
    slope_run = slope_final[0][0]
    edge_angle1 = angle_final[0]

    true_center = np.array([110, 110])
    center_offset = int(np.ceil(np.max(np.abs(center - true_center))))
    if center_offset < 15:
        center_offset = 15
    edge_start = (-slope_rise/slope_run)
    edge_end = ((110 - center_offset) - slope_rise) / slope_run
    edge_width = center_offset * np.tan(30 * np.pi / 180)

    return [np.int(np.floor(edge_start)), np.int(np.ceil(edge_end)), np.int(np.floor(edge_width)),
            int(110 - 2 * center_offset), edge_angle1]

def contrast_check(img):
    # Turn Grayscale
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    nlin, npix = img.shape

    # ensure a 'positive' edge at the vertical: compare the sum of the first n cols at each side
    tleft = np.average(img[:, 0:5])  # test
    tright = np.average(img[:, npix - 6:npix])  # test

    # Test for low contrast edge
    test = abs((tleft - tright) / (tleft + tright))
    if test < 0.2:
        status = 1
        '''
        print(' ** WARNING: Edge contrast is less that 20%, this can')
        print('             lead to high error in the SFR measurement.')
        '''
        return False
    return True


def sfr(img, angle, pix_size=1.75, verbose=False):
    # This function calls the sfr function

    # Turn Grayscale
    if len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    nlin, npix = img.shape
    slope = np.tan(angle)
    angle_deg = 180 * angle / np.pi
    if verbose:
        print('Edge angle: {0:.3f} degrees'.format(angle_deg))
        print('Slope: {0:.3f}'.format(slope))

    status, esf_x, esf_y, esf_fit_x, esf_fit_y = project(img, angle, pix_size, verbose)
    if status == 0:
        mtf, freq, psf = fft(esf_fit_x, esf_fit_y, nlin, pix_size, verbose)
    else:
        psf = np.ones(len(esf_fit_x)) * np.nan
        mtf = np.ones(len(esf_fit_x) / nlin) * np.nan
        freq = np.ones(len(esf_fit_x) / nlin) * np.nan

    return status, freq, mtf, esf_x, esf_y, esf_fit_x, esf_fit_y, psf


def fft(esf_x, esf_y, lines, pix_size = 1.75, verbose=False):
    density = pix_size / 1000
    n = len(esf_x)
    mid = np.int(n / 2)

    # Two FIR filter types
    fil1 = [0.5, -0.5]

    # compute lsf (line spread function) first derivative via FIR (1x3) filter fil
    psf = np.convolve(fil1, esf_y)
    psf[0] = psf[1]  # d is longer by 1
    psf[-1] = psf[-2]

    # Center the psf
    maxi = np.argmax(psf)
    psf = np.roll(psf, mid - maxi)
    psf = psf[1:]

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('PSF')
        plt.scatter(esf_x, psf)
        plt.show()

    nn2out = round(n / lines) + 1  # dont ask about +1
    # Compute Fourier transform, scale and correct for FIR filter response
    fft_res = abs(np.fft.fft(psf, int(n)))  # compute fft
    mtf = fft_res[0:mid] / fft_res[0]  # normalize to the 0 frequency value
    mtf = mtf * fir2fix(mid, 3)  # FIR filter correction
    mtf = 100 * mtf[0:nn2out]  # put mtf in %

    freq = np.zeros(n)
    for i in range(0, int(n)):
        freq[i] = float(lines * i) / (density * n);
    freq = freq[0:nn2out]  # units are lines/mm

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('MTF')
        plt.scatter(freq, mtf)
        plt.show()

    return mtf, freq, psf


def project(img, angle_rad, pix_size = 1.75, verbose=False):
    # angle_rad = np.arcsin(1/np.sqrt(1+(1/slope)**2))
    # angle_rad = np.arctan(slope)
    angle_deg = 180 * angle_rad / np.pi

    if verbose:
        print('MTF PROJECT: Angle: ' + str(np.round(angle_deg, 2)))

    [nlin, npix] = img.shape

    x_array = []
    y_array = []

    for n in range(0, nlin):
        for m in range(0, npix):
            x = pix_size / 2 + pix_size * m + (pix_size / 2 + n * pix_size) * np.tan(angle_rad)
            y = img[n, m]
            x_array.append(x)
            y_array.append(y)

    esf = np.array([x_array, y_array]).T
    esf = esf[esf[:, 0].argsort()]
    x_array = np.round(esf.T[0], 3)
    y_array = esf.T[1]


    '''
    # Lowess Fit
    lowess = sm.nonparametric.lowess
    frac = 1. /(npix)
    lowess_fit = lowess(y_array, x_array, frac = frac, it = 10)
    lowess_fit = np.array(lowess_fit)

    status = 0
    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('ESF')
        plt.scatter(x_array, y_array)
        plt.plot(lowess_fit.T[0], lowess_fit.T[1], '-', c='r', lw=2)
        plt.show()

    # Three Sigmoid Fit
    x_sig = np.arange(0, npix * pix_size, pix_size / nlin)
    status = 0
    try:
        popt, pcov = curve_fit(three_sigmoid, x_array, y_array, bounds=(
            0, [npix * pix_size, 255, 255, 2, 255, 2, npix * pix_size, 255, 2, npix * pix_size]))
        y_sig = three_sigmoid(x_sig, popt[0], popt[1], popt[2], popt[3], popt[4], popt[5], popt[6], popt[7], popt[8],
                              popt[9])
    except RuntimeError:
        status = 1
        y_sig = np.zeros(len(x_sig))

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('ESF')
        plt.scatter(x_array, y_array)
        plt.plot(x_sig, y_sig, '-', c='r', lw=2)
        plt.show()
    '''
    # Interpolation
    status = 0
    x_sig = np.arange(0, npix * pix_size, pix_size / nlin)
    y_interp = np.interp(x_sig, x_array, y_array)

    if verbose:
        fig = plt.figure()
        fig.canvas.set_window_title('ESF')
        plt.scatter(x_array, y_array)
        plt.plot(x_sig, y_interp, '-', c='r', lw=2)
        plt.show()

    # return status, x_array, y_array, lowess_fit.T[0], lowess_fit.T[1]
    return status, x_array, y_array, x_sig, y_interp

def perp(a):
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b


# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return

def fir2fix(n, m):
    correct = np.ones(n)
    m -= 1
    scale = 1
    for i in range(2, np.int(n)):
        a1 = np.pi * i * m / (2 * (n + 1))
        correct[i] = a1 / np.sin(a1)
        correct[i] = 1 + scale * (correct[i] - 1)
        if correct[i] > 10:  # Note limiting the correction to the range [1, 10]
            correct[i] = 10

    return correct


def seg_intersect(a1, a2, b1, b2):
    da = a2 - a1
    db = b2 - b1
    dp = a1 - b1
    dap = perp(da)
    denom = np.dot(dap, db)
    num = np.dot(dap, dp)
    return (num / denom.astype(float)) * db + b1

def createLineIterator(P1, P2, img):
    """
    Produces and array that consists of the coordinates and intensities of each pixel in a line between two points

    Parameters:
        -P1: a numpy array that consists of the coordinate of the first point (x,y)
        -P2: a numpy array that consists of the coordinate of the second point (x,y)
        -img: the image being processed

    Returns:
        -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])
    """
    # define local variables for readability
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    # difference and absolute difference between points
    # used to calculate slope and relative location between points
    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    # predefine numpy array for output based on distance between points
    itbuffer = np.empty(shape=(np.maximum(dYa, dXa), 3), dtype=np.float32)
    itbuffer.fill(np.nan)

    # Obtain coordinates along the line using a form of Bresenham's algorithm
    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X:  # vertical line segment
        itbuffer[:, 0] = P1X
        if negY:
            itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
        else:
            itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
    elif P1Y == P2Y:  # horizontal line segment
        itbuffer[:, 1] = P1Y
        if negX:
            itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
        else:
            itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
    else:  # diagonal line segment
        steepSlope = dYa > dXa
        if steepSlope:
            slope = dX.astype(np.float32) / dY.astype(np.float32)
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
            itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
        else:
            slope = dY.astype(np.float32) / dX.astype(np.float32)
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
            itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

            # Remove points outside of image
    colX = itbuffer[:, 0]
    colY = itbuffer[:, 1]
    itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

    # Get intensities from img ndarray
    itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint8), itbuffer[:, 0].astype(np.uint8)]

    return itbuffer[:, 2]

def three_sigmoid(x, a, b, c, d, e, f, g, h, i, j):
    return b / (1 + np.e ** (-d * (x - a))) + c + e / (1 + np.e ** (-f * (x - g))) + h / (1 + np.e ** (-i * (x - j)))
