# -*- coding: utf-8 -*-
"""
Created on Mon Sep 26 23:16:28 2016

@author: tim tan

Base class for Toshiba serial commands

Sample usage:
# extend this class to include custom functions
class ToshibaSerialCommandExtended(ToshibaSerialCommandBase):
  def DoFancyStuff(self):
    # call base commands
    self.SetGainMode(self.GAIN_MODE_MANUAL)
    self.SetGain(6.0)

# instantiate extended class
t = ToshibaSerialCommandExtended('COM9')

# do fancy stuff
t.DoFancyStuff()

# delete instance to close serial port
del t

S-Y Changes 161213;
(0) Added Joint Board Access Capability

S-Y Changes 160711;
(0) RunCommand now returns 'bufout' 
    - useful for extracting whether the correct values were sent
    - passes 'get' commands
(1) All 'get' commands had a value specified, this has been removed;
    - No sense in specifying value to go get
    - RunCommand has been modified to inclue a special case for Get commands
        - bufin[2],bufin[3] not specified, so cannot ever match bufout[2],bufout[3]
(2) 'Exposure' get and set have been changed to ShutterSpeedManual 
    - Shutter Speed has been changed to ShutterSpeedAuto

S-Y Changes 171201;
(0) Turned off Flow Control, allows passing parameters 0x11 and 0x13





"""

import serial
import sys


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """

    ports = ['COM%s' % (i + 1) for i in range(256)]
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class ToshibaCCU:
    def __init__(self, verbose = False):
        self.error = False
        self.verbose = verbose

        ports = serial_ports()
        print('\nCom Ports: ' + str(ports))
        port = []
        found = False
        self.searching = True

        for i in ports:

            self.ser = serial.Serial(
                port = i,
                baudrate=19200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                xonxoff=False,
                timeout=0.3,
                writeTimeout=0.3
            )
            #self.sio = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser), encoding='utf-8', newline='\r\n')

            # change Aurora serial setting to 921600 baud and close port
            stat = self.GetCCU()
            if stat == 1:
                self.port = i
                self.ser.close()
                found = True
                break
            elif stat == 2:
                self.port = i
                self.SetBaud(0)
                self.ser.close()
                found = True
                break
            self.ser.close()


        if not found:
            print('CCU not Found, Exiting')
            sys.exit()
        else:
            print('\nCCU found on port '+str(self.port)+'\n')
        self.searching = False
        self.ser = serial.Serial(
            port=self.port,
            baudrate=19200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False,
            timeout=0.3,
            writeTimeout=0.3
        )

        # Insert check Serial Baud, try other baud if wrong

    def __del__(self):
        self.ser.close()

    def changeBaud(self):
        if self.ser.baudrate == 9600:
            self.ser.close()
            self.ser = serial.Serial(
                port=self.port,
                baudrate=19200,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                xonxoff=False,
                timeout=0.3,
                writeTimeout=0.3
            )
        else:
            self.ser.close()
            self.ser = serial.Serial(
                port=self.port,
                baudrate=9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                xonxoff=False,
                timeout=0.3,
                writeTimeout=0.3
            )

    def close(self):
        self.__del__()

    def RunCommand(self, bufin, get = False, getData = False, baudchange = False):
        # print('Sending: '+str(bufin))
        status = 1
        try:
            r = self.ser.write(bytearray(bufin))
        except(Exception):
            r = 0

        if r != 4:
            if not self.searching:
                print('Serial write failed!')
            status = 0
            return status, bufin

        if baudchange:
            self.changeBaud()
            try:
                r = self.ser.write(bytearray(bufin))
            except(Exception):
                r = 0

            if r != 4:
                print('Serial write failed!')
                status = 0
                print(r)
                return

        try:
            bufout = self.ser.read(4)
        except(Exception):
            bufout = ''
            status = 0
        # print('Received: '+str(bufout))

        # Success is 0x06 + REQUEST[1:]
        # (SY - 161107) Changed the condition for acceptance, for 'get' commands, bufin 2,3 not specified
        if (len(bufout) == 4 and
                    get == False and
                    bufout[0] == 0x06 and
                    bufout[1] == bufin[1] and
                    bufout[2] == bufin[2] and
                    bufout[3] == bufin[3]):
            status = 1
            return status, bufout

        elif (len(bufout) == 4 and
                      get == True and
                      bufout[0] == 0x06 and
                      bufout[1] == bufin[1]):
            status = 1
            return status, bufout

        elif (len(bufout) == 4 and
                      getData == True and
                      bufout[0] == 0x06 and
                      bufout[1] == bufin[1] and
                      bufout[2] == bufin[2]):
            status = 1
            return status, bufout

        # Error is 0x15 + ERROR NUM + 0xff + 0xff
        elif (len(bufout) == 4 and
                      bufout[0] == 0x15 and
                      bufout[2] == 0xff and
                      bufout[3] == 0xff):
            print('CCU returns error ' + str(bufout[1]) + '!')

        else:
            if not self.searching:
                print('Length of Bytes Array received: ' + str(len(bufout)))
                print('Buffer out: ' + str(bufout))
                print('Unknown error occurred! If 0 received, try changing the portBaud!')
            if len(bufout) == 0:
                status = 0
            else:
                status = 2
            return status, bufout

    def SetRegister(self, addr, val, baudchange = False):

        if addr < 0 or addr > 0xff or val > 0xffff or val < -0x8000:
            print('Invalid addr/val Specified to SetRegister!')
            print(addr)
            return

        # Request is 0x53 + ADDR + DATA(L) + DATA(H)
        bufin = [0] * 4
        bufin[0] = 0x53
        bufin[1] = addr
        bufin[2] = val & 0xff
        bufin[3] = (val >> 8) & 0xff

        return self.RunCommand(bufin, baudchange = baudchange)

    def GetRegister(self, addr):

        if (addr < 0 or addr > 0xff):
            print('Invalid addr specified to GetRegister!')
            print(addr)
            return

        # Success is 0x06 + REQUEST[1:]
        bufin = [0] * 4
        bufin[0] = 0x05
        bufin[1] = addr
        bufin[2] = 0xff
        bufin[3] = 0xff

        return self.RunCommand(bufin, get=True)

    def SetDo(self, cmd):

        # Request is 0x4b + CMD + 0xff + 0xff
        bufin = [0] * 4
        bufin[0] = 0x4b
        bufin[1] = cmd
        bufin[2] = 0xff
        bufin[3] = 0xff

        return self.RunCommand(bufin)

    def SetData(self, dat_addr, dat_val=0xff):

        if (dat_addr < 0 or dat_addr > 0x4ff):
            print('Invalid address specified to SetData!')
            print(dat_addr)
            return

        if (dat_val < -255 or dat_val > 255):
            print('Invalid value specified to SetData!')
            print(dat_val)
            return

            # Write is 0x45 + ADDR(L) + ADDR(H) + DATA(H)
        bufin = [0] * 4
        bufin[0] = 0x43
        bufin[1] = dat_addr & 0xff
        bufin[2] = (dat_addr >> 8) & 0xff
        bufin[3] = dat_val & 0xff

        return self.RunCommand(bufin)

    def GetData(self, dat_addr):

        if (dat_addr < 0 or dat_addr > 0x4ff):
            print('Invalid address specified to GetData!')
            print(dat_addr)
            return

        # Write is 0x43 + ADDR(L) + ADDR(H) + 0xff
        bufin = [0] * 4
        bufin[0] = 0x45
        bufin[1] = dat_addr & 0xff
        bufin[2] = (dat_addr >> 8) & 0xff
        bufin[3] = 0xff

        return self.RunCommand(bufin, getData=True)[1][3]

    # Push Button Commands
    def SetDoDisp(self):
        self.SetDo(0x01)

    def SetDoPage(self):
        self.SetDo(0x02)

    def SetDoMenuup(self):
        self.SetDo(0x03)

    def SetDoMenudown(self):
        self.SetDo(0x04)

    def SetDoDataup(self):
        self.SetDo(0x05)

    def SetDoDatadown(self):
        self.SetDo(0x06)

    def SetDoAwb(self):
        self.SetDo(0x07)

    def SetDoAbb(self):
        self.SetDo(0x08)

    def SetDoPreset(self):
        self.SetDo(0x0a)

    def SetDoFile(self):
        self.SetDo(0x0b)

    def SetDoLockon(self):
        self.SetDo(0x0d)

    def SetDoLockoff(self):
        self.SetDo(0x0e)

    def SetDoFreeze(self):
        self.SetDo(0x0c)

    def GetCCU(self):
        stat, reply = self.GetRegister(0x01)
        return stat

    def SetScene(self, val_int):
        scene = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E'}
        if not val_int in scene.keys():
            print("Invalid argument to SetScene!")
            return
        back = self.SetRegister(0x90, val_int)
        if self.verbose and back[0] == 1:
            print('Setting SHUTTER to ' + str(scene[val_int]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SHUTTER to ' + str(scene[val_int]) + ' has Failed!')

    def GetScene(self):
        scene = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E'}
        back = self.GetRegister(0x90)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('SCENE set to ' + str(scene[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SCENE has Failed!')

    def SetShutterMode(self, val_SHUTTER_MODE):
        shutter = {0: 'AUTO', 1: 'MANUAL', 2: 'SS'}
        if not val_SHUTTER_MODE in shutter.keys():
            print("Invalid argument to SetShutterMode!")
            return
        back = self.SetRegister(0x01, val_SHUTTER_MODE)
        if self.verbose and back[0] == 1:
            print('Setting SHUTTER to ' + str(shutter[val_SHUTTER_MODE]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SHUTTER to ' + str(shutter[val_SHUTTER_MODE]) + ' has Failed!')

    def GetShutterMode(self):
        shutter = {0: 'AUTO', 1: 'MANUAL', 2: 'SS'}
        back = self.GetRegister(0x01)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('SHUTTER set to ' + str(shutter[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SHUTTER MODE has Failed!')


    def SetShutterLevel(self, val_int=0):
        if val_int < -100 or val_int > 100:
            print("FAULT: Invalid argument to SetShutterLevel!")
            return
        elif not self.GetShutterMode() == 0:
            print("FAULT: Wrong SHUTTER MODE, please switch to AUTO!")
            return
        back = self.SetRegister(0x03, val_int)
        if self.verbose and back[0] == 1:
            print('Setting SHUTTER LEVEL to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SHUTTER LEVEL to ' + str(val_int + ' has Failed!'))

    def GetShutterLevel(self):
        back = self.GetRegister(0x03)
        if back[0] == 1:
            val = back[1][2]

            if val < 101:
                val = val
            else:
                val = -1 * (256 - val)
            return val
        elif self.verbose and back[0] == 1:
            val = back[1][2]

            if val < 101:
                val = val
            else:
                val = -1 * (256 - val)

            print('SHUTTER LEVEL set to ' + str(val))
            return val
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SHUTTER LEVEL has Failed!')

    def SetShutterPeakave(self, val_int=5):
        if val_int < 0 or val_int > 10:
            print("FAULT: Invalid argument to SetShutterPeakave! Set a value 0-10!")
            return
        elif not self.GetShutterMode() == 0:
            print("FAULT: Wrong SHUTTER MODE, please switch to AUTO!")
            return
        back = self.SetRegister(0x04, val_int)
        if self.verbose and back[0] == 1:
            print('Setting SHUTTER PEAKAVE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SHUTTER PEAKAVE to ' + str(val_int + ' has Failed!'))

    def GetShutterPeakave(self):
        back = self.GetRegister(0x04)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            val = back[1][2]
            print('SHUTTER PEAKAVE set to ' + str(val))
            return val
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SHUTTER PEAKAVE has Failed!')

    def SetShutterSpeedAuto(self, val_int=10):
        if val_int < 0 or val_int > 19:
            print("FAULT: Invalid argument to SetShutterSpeed! Please Select 0-19")
            return
        elif not self.GetShutterMode() == 0:
            print("FAULT: Wrong SHUTTER MODE, please switch to AUTO!")
            return
        back = self.SetRegister(0x05, val_int)
        if self.verbose and back[0] == 1:
            print('Setting SHUTTER AUTO SPEED to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SHUTTER AUTO SPEED to ' + str(val_int) + ' has Failed!')

    def GetShutterSpeedAuto(self):
        back = self.GetRegister(0x05)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            val = back[1][2]
            print('SHUTTER AUTO SPEED set to ' + str(val))
            return val
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SHUTTER AUTO SPEED has Failed!')

    def SetShutterArea(self, val_AREA = 0):
        area = {0: 'PREA', 1: 'PREA', 2: 'PREA', 3: 'PREA', 4: 'PREA', 5: 'PREA'}
        if not val_AREA in area.keys():
            print("Invalid argument to SetShutterArea!")
            return
        back = self.SetRegister(0x06, val_AREA)
        if self.verbose and back[0] == 1:
            print('Setting SHUTTER AREA to ' + str(area[val_AREA]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SHUTTER AREA to ' + str(area[val_AREA]) + ' has Failed!')

    def GetShutterArea(self):
        area = {0: 'PREA', 1: 'PREA', 2: 'PREA', 3: 'PREA', 4: 'PREA', 5: 'PREA'}
        back = self.GetRegister(0x06)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('SHUTTER AREA set to ' + str(area[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SHUTTER AREA has Failed!')


    # Custom Defined Area
    def SetCustomArea(self, value=0x1818):
        if value > 0xffff or value < -0x8000:
            print("Innapropriate Auto Shutter Area Selected")
            return
        self.SetRegister(0x07, value)
        self.SetRegister(0x08, 0xff3c)
        self.SetRegister(0x09, 0x3cff)
        self.SetRegister(0x0a, value)
        if self.verbose:
            print('Setting Shutter Custom Area')

    def GetCustomArea(self):
        return self.GetRegister(0x07)

    # Manual Shutter Commands
    def SetShutterSpeedManual(self, val):
        shutterSpeed = {0: '1.0 / 60',
                        1: '1.0 / 100',
                        2: '1.0 / 125',
                        3: '1.0 / 250',
                        4: '1.0 / 500',
                        5: '1.0 / 1000',
                        6: '1.0 / 2000',
                        7: '1.0 / 3000',
                        8: '1.0 / 4000',
                        9: '1.0 / 6500',
                        10: '1.0 / 13000'}

        if not self.GetShutterMode() == 1:
            print("Wrong shutter mode, please switch to Manual Shutter!")
            return

        elif val < 0 or val > 10:
            print('Invalid Parameter for Shutter Speed, Please enter a value 0-10')
            return

        else:
            back = self.SetRegister(0x0d, val)
            if self.verbose and back[0] == 1:
                print('Setting SHUTTER AREA to ' + str(shutterSpeed[val]))
            elif self.verbose and back[0] == 0:
                print('FAULT: Setting SHUTTER AREA to ' + str(shutterSpeed[val]) + ' has Failed!')

    def GetShutterSpeedManual(self, val = 0):
        shutterSpeed = {0: '1.0 / 60',
                        1: '1.0 / 100',
                        2: '1.0 / 125',
                        3: '1.0 / 250',
                        4: '1.0 / 500',
                        5: '1.0 / 1000',
                        6: '1.0 / 2000',
                        7: '1.0 / 3000',
                        8: '1.0 / 4000',
                        9: '1.0 / 6500',
                        10: '1.0 / 13000'}

        if not self.GetShutterMode() == 1:
            print("Wrong shutter mode, please switch to Manual Shutter!")
            return
        else:
            back = self.GetRegister(0x0d)
            if back[0] == 1:
                return back[1][2]
            elif self.verbose and back[0] == 1:
                val = back[1][2]
                print('SHUTTER MANUAL SPEED set to ' + str(shutterSpeed[val]))
                return shutterSpeed[val]
            elif self.verbose and back[0] == 0:
                print('FAULT: Reading SHUTTER AUTO SPEED has Failed!')


    # Shutter Sync Commands
    def SetShutterSync(self, val_int):
        if val_int < 0 or val_int > 218:
            print("Invalid argument to SetShutterSync!")
            return
        elif not self.GetShutterMode() == 2:
            print("Wrong shutter mode, please switch to Shutter Sync!")
            return
        else:
            back = self.SetRegister(0x0e, val_int)
            if self.verbose and back[0] == 1:
                val = back[1][2]
                print('SHUTTER SYNC set to ' + str(val))
                return val_int
            elif self.verbose and back[0] == 0:
                print('FAULT: Reading SHUTTER SYNC has Failed!')

    def GetShutterSync(self):

        back = self.GetRegister(0x0e)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            val = back[1][2]
            print('SHUTTER SYNC set to ' + str(val))
            return val
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SHUTTER SYNC has Failed!')

    ###################################################################################
    def SetGainMode(self, val_GAIN_MODE):
        gain = {0: 'AUTO', 1: 'MANUAL', 2: 'OFF'}
        if not val_GAIN_MODE in gain.keys():
            print("Invalid argument to SetShutterMode!")
            return
        back = self.SetRegister(0x20, val_GAIN_MODE)
        if self.verbose and back[0] == 1:
            print('Setting GAIN to ' + str(gain[val_GAIN_MODE]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GAIN to ' + str(gain[val_GAIN_MODE]) + ' has Failed!')

    def GetGainMode(self):
        gain = {0: 'AUTO', 1: 'MANUAL', 2: 'OFF'}
        back = self.GetRegister(0x20)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GAIN set to ' + str(gain[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GAIN MODE has Failed!')

    def SetMaxGain(self, val):
        if val > 18 or val < 0:
            print("Invalid argument to SetMaxGain! Enter Value 0-18!")
            return
        elif not self.GetGainMode() == 0:
            print("FAULT: Wrong GAIN MODE, please switch to AUTO!")
            return
        back = self.SetRegister(0x21, val)
        if self.verbose and back[0] == 1:
            print('Setting MAX GAIN to ' + str(val))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MAX GAIN to ' + str(val + ' has Failed!'))

    def GetMaxGain(self):
        back = self.GetRegister(0x21)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('MAX GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MAX GAIN has Failed!')

    def SetManGain(self, val):
        if val > 18 or val < 0:
            print("Invalid argument to SetManGain! Enter Value 0-18!")
            return
        elif not self.GetGainMode() == 1:
            print("FAULT: Wrong GAIN MODE, please switch to MANUAL!")
            return
        back = self.SetRegister(0x22, val)
        if self.verbose and back[0] == 1:
            print('Setting MANUAL GAIN  to ' + str(val))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MANUAL GAIN to ' + str(val + ' has Failed!'))

    def GetManGain(self):
        back = self.GetRegister(0x22)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('MANUAL GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MANUAL GAIN has Failed!')

    ##################################################################################
    def SetWbMode(self, val_WB_MODE):
        wb = {0: 'AUTO', 2: 'MANUAL'}
        if not val_WB_MODE in wb.keys():
            print("Invalid argument to SetWbMode!")
            return
        back = self.SetRegister(0x30, val_WB_MODE)
        if self.verbose and back[0] == 1:
            print('Setting WB MODE to ' + str(wb[val_WB_MODE]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting WB MODE to ' + str(wb[val_WB_MODE]) + ' has Failed!')

    def GetWbMode(self):
        wb = {0: 'AUTO', 2: 'MANUAL'}
        back = self.GetRegister(0x30)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('WB MODE set to ' + str(wb[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading WB MODE has Failed!')

    def SetWbCtemp(self, val_WB_CTEMP):
        wbColor = {0: '3200K', 1: '5600K'}
        if not val_WB_CTEMP in wbColor.keys():
            print("Invalid argument to SetShutterMode!")
            return
        back = self.SetRegister(0x31, val_WB_CTEMP)
        if self.verbose and back[0] == 1:
            print('Setting WB COLOR to ' + str(wbColor[val_WB_CTEMP]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting WB COLOR to ' + str(wbColor[val_WB_CTEMP]) + ' has Failed!')

    def GetWbCtemp(self):
        wbColor = {0: '3200K', 1: '5600K'}
        back = self.GetRegister(0x31)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('WB COLOR set to ' + str(wbColor[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading WB COLOR has Failed!')

    # Auto commands
    def SetWbRpaint(self, val_int=0):
        if val_int < -10 or val_int > 10:
            print("Invalid argument to SetWbRpaint! Please set value -10-10!")
            return
        elif not self.GetWbMode() == 0:
            print("FAULT: Wrong WB MODE, please switch to AUTO!")
            return
        back = self.SetRegister(0x32, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RPAINT to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RPAINT to ' + str(val_int + ' has Failed!'))

    def GetWbRpaint(self):

        back = self.GetRegister(0x32)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RPAINT set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RPAINT has Failed!')

    def SetWbBpaint(self, val_int):
        if val_int < -10 or val_int > 10:
            print("Invalid argument to SetWbBpaint! Please set value -10-10!")
            return
        elif not self.GetWbMode() == 0:
            print("FAULT: Wrong WB MODE, please switch to AUTO!")
            return
        back = self.SetRegister(0x33, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BPAINT to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BPAINT to ' + str(val_int + ' has Failed!'))

    def GetWbBpaint(self):
        back = self.GetRegister(0x33)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BPAINT set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BPAINT has Failed!')

    def SetWbArea(self, val_AREA = 0):
        area = {0: 'PREA', 1: 'PREA', 2: 'PREA', 3: 'PREA', 4: 'PREA', 5: 'PREA'}
        if not val_AREA in area.keys():
            print("Invalid argument to SetWBArea!")
            return
        back = self.SetRegister(0x34, val_AREA)
        if self.verbose and back[0] == 1:
            print('Setting WB AREA to ' + str(area[val_AREA]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting WB AREA to ' + str(area[val_AREA]) + ' has Failed!')

    def GetWbArea(self):
        area = {0: 'PREA', 1: 'PREA', 2: 'PREA', 3: 'PREA', 4: 'PREA', 5: 'PREA'}
        back = self.GetRegister(0x34)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('WB AREA set to ' + str(area[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading WB AREA has Failed!')

    def GetWbRread(self):
        back = self.GetRegister(0xa0)
        a = back[2:][::-1]
        b = a[0]
        c = a[1]
        if back[0] == 1:
            return 256*b+c
        elif self.verbose and back[0] == 1:
            print('R READ reports ' + str(256 * b + c))
            return 256 * b + c
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading R READ has Failed!')

    def GetWbBread(self):
        back = self.GetRegister(0xa1)
        a = back[2:][::-1]
        b = a[0]
        c = a[1]
        if back[0] == 1:
            return 256*b+c
        elif self.verbose and back[0] == 1:
            print('B READ reports ' + str(256 * b + c))
            return 256 * b + c
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading B READ has Failed!')

    # Manual Commands
    def SetWbRgain(self, val_int):
        if val_int < -100 or val_int > 100:
            print("Invalid argument to SetWbRgain!")
            return
        elif not self.GetWbMode() == 2:
            print("FAULT: Wrong WB MODE, please switch to MANUAL!")
            return
        back = self.SetRegister(0x39, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RGAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RGAIN to ' + str(val_int + ' has Failed!'))

    def GetWbRgain(self):

        back = self.GetRegister(0x39)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RGAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RGAIN has Failed!')

    def SetWbBgain(self, val_int):
        if val_int < -100 or val_int > 100:
            print("Invalid argument to SetWbBgain!")
            return
        elif not self.GetWbMode() == 2:
            print("FAULT: Wrong WB MODE, please switch to MANUAL!")
            return
        back = self.SetRegister(0x3a, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BGAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BGAIN to ' + str(val_int + ' has Failed!'))

    def GetWbBgain(self):
        back = self.GetRegister(0x3a)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BGAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BGAIN has Failed!')

    ########################################################################################
    def SetGammaMode(self, val_ENABLE):
        gamma = {0: 'ON', 1: 'OFF'}
        if not val_ENABLE in gamma.keys():
            print("Invalid argument to SetGammaMode!")
            return
        back = self.SetRegister(0x40, val_ENABLE)
        if self.verbose and back[0] == 1:
            print('Setting GAMMA MODE to ' + str(gamma[val_ENABLE]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GAMMA MODE to ' + str(gamma[val_ENABLE]) + ' has Failed!')

    def GetGammaMode(self):
        gamma = {0: 'ON', 1: 'OFF'}
        back = self.GetRegister(0x40)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GAMMA MODE set to ' + str(gamma[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GAMMA MODE has Failed!')

    def SetGamma(self, val_int):
        if val_int < -10 or val_int > 10:
            print("Invalid argument to SetGamma!")
            return
        elif not self.GetGammaMode() == 0:
            print("FAULT: Wrong GAMMA MODE, please switch to ON!")
            return
        back = self.SetRegister(0x41, val_int)
        if self.verbose and back[0] == 1:
            print('Setting GAMMA to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GAMMA to ' + str(val_int + ' has Failed!'))

    def GetGamma(self):
        back = self.GetRegister(0x41)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GAMMA MODE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GAMMA MODE has Failed!')


    def SetBlackgamma(self, val_BLACKGAMMA):
        Bgamma = {0: 'LOW', 1: 'NORMAL', 2: 'HIGH'}
        if not val_BLACKGAMMA in Bgamma.keys():
            print("Invalid argument to SetBlackgamma!")
            return
        back = self.SetRegister(0x42, val_BLACKGAMMA)
        if self.verbose and back[0] == 1:
            print('Setting BLACK GAMMA MODE to ' + str(Bgamma[val_BLACKGAMMA]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BLACK GAMMA MODE to ' + str(Bgamma[val_BLACKGAMMA]) + ' has Failed!')


    def GetBlackgamma(self):
        Bgamma = {0: 'LOW', 1: 'NORMAL', 2: 'HIGH'}
        back = self.GetRegister(0x42)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BLACK GAMMA set to ' + str(Bgamma[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BLACK GAMMA has Failed!')


    def SetMped(self, val_int):
        if val_int < -200 or val_int > 200:
            print("Invalid argument to SetMped! Please Enter Value -200 to 200")
            return
        back = self.SetRegister(0x46, val_int)
        if self.verbose and back[0] == 1:
            print('Setting MASTER PEDESTAL to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MASTER PEDESTAL to ' + str(val_int + ' has Failed!'))

    def GetMped(self):
        back = self.GetRegister(0x46)
        val = back[1][2:]
        if val[1] == 255:
            num = -(256 - val[0])
        else:
            num = val[0]

        if back[0] == 1:
            return num
        elif self.verbose and back[0] == 1:
            print('MASTER PEDESTAL set to ' + str(num))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MASTER PEDESTAL has Failed!')

    def SetRped(self, val_int):
        if val_int < -100 or val_int > 100:
            print("Invalid argument to SetMped! Please Enter Value -200 to 200")
            return
        back = self.SetRegister(0x49, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RED PEDESTAL to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RED PEDESTAL to ' + str(val_int + ' has Failed!'))


    def GetRped(self):
        back = self.GetRegister(0x49)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RED PEDESTAL set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RED PEDESTAL has Failed!')

    def SetBped(self, val_int):
        if val_int < -100 or val_int > 100:
            print("Invalid argument to SetBped! Please Enter Value -100 to 100")
            return
        back = self.SetRegister(0x4a, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BLUE PEDESTAL to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BLUE PEDESTAL to ' + str(val_int + ' has Failed!'))

    def GetBped(self):
        back = self.GetRegister(0x4a)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BLUE PEDESTAL set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BLUE PEDESTAL has Failed!')

    ########################################DTL####################################

    def SetDTLGain(self, val_int):
        if val_int < -0 or val_int > 31:
            print("Invalid argument to DTL Gain! Please Enter Value 0 to 31")
            return
        back = self.SetRegister(0x44, val_int)
        if self.verbose and back[0] == 1:
            print('Setting DTL GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting DTL GAIN to ' + str(val_int + ' has Failed!'))

    def GetDTLGain(self):
        back = self.GetRegister(0x44)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('DTL GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading DTL GAIN has Failed!')

    def SetDTLFreq(self, val_int):
        if val_int < 1 or val_int > 16:
            print("Invalid argument to DTL Freq! Please Enter Value 1 to 16")
            return
        back = self.SetRegister(0x4b, val_int)

        if self.verbose and back[0] == 1:
            print('Setting DTL FREQ to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting DTL FREQ to ' + str(val_int + ' has Failed!'))

    def GetDTLFreq(self):
        back = self.GetRegister(0x4b)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('DTL FREQ set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading DTL FREQ has Failed!')

    def SetDNR(self, val_int):
        dnr = {0: 'OFF', 1: 'LOW', 2:'HIGH'}
        if not val_int in dnr.keys():
            print("Invalid argument to SetDNR!")
            return
        back = self.SetRegister(0x48, val_int)
        if self.verbose and back[0] == 1:
            print('Setting DNR to ' + str(dnr[val_int]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting DNR to ' + str(dnr[val_int]) + ' has Failed!')

    def GetDNR(self):
        dnr = {0: 'OFF', 1: 'LOW', 2: 'HIGH'}
        back = self.GetRegister(0x48)
        if self.verbose and back[0] == 1:
            print('DNR set to ' + str(dnr[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading DNR has Failed!')

    ###################CCM########################################################
    def SetMatrix(self, val_ENABLE):
        ccm = {0: 'ON', 1: 'OFF'}
        if not val_ENABLE in ccm.keys():
            print("Invalid argument to SetMatrix!")
            return
        back = self.SetRegister(0x50, val_ENABLE)
        if self.verbose and back[0] == 1:
            print('Setting CCM to ' + str(ccm[val_ENABLE]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting CCM to ' + str(ccm[val_ENABLE]) + ' has Failed!')

    def GetMatrix(self):
        ccm = {0: 'ON', 1: 'OFF'}
        back = self.GetRegister(0x50)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('CCM set to ' + str(ccm[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading CCM has Failed!')

    def SetRhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetRhue!")
            return
        back = self.SetRegister(0x51, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RED HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RED HUE to ' + str(val_int + ' has Failed!'))

    def GetRhue(self):
        back = self.GetRegister(0x51)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RED HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RED HUE has Failed!')

    def SetRgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetRgain!")
            return
        back = self.SetRegister(0x52, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RED GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RED GAIN to ' + str(val_int + ' has Failed!'))

    def GetRgain(self):
        back = self.GetRegister(0x52)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RED GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RED GAIN has Failed!')

    def SetYhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetYhue!")
            return
        back = self.SetRegister(0x57, val_int)
        if self.verbose and back[0] == 1:
            print('Setting YELLOW HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting YELLOW HUE to ' + str(val_int + ' has Failed!'))
    def GetYhue(self):
        back = self.GetRegister(0x57)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('YELLOW HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading YELLOW HUE has Failed!')

    def SetYgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetYgain!")
            return
        back = self.SetRegister(0x58, val_int)
        if self.verbose and back[0] == 1:
            print('Setting YELLOW GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting YELLOW GAIN to ' + str(val_int + ' has Failed!'))

    def GetYgain(self):
        back = self.GetRegister(0x58)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('YELLOW GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading YELLOW GAIN has Failed!')

    def SetGhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetGhue!")
            return
        back = self.SetRegister(0x53, val_int)
        if self.verbose and back[0] == 1:
            print('Setting GREEN HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GREEN HUE to ' + str(val_int + ' has Failed!'))

    def GetGhue(self):
        back = self.GetRegister(0x53)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GREEN HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GREEN HUE has Failed!')

    def SetGgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetGgain!")
            return
        back = self.SetRegister(0x54, val_int)
        if self.verbose and back[0] == 1:
            print('Setting GREEN GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GREEN GAIN to ' + str(val_int + ' has Failed!'))

    def GetGgain(self):
        back = self.GetRegister(0x54)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GREEN GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GREEN GAIN has Failed!')

    def SetChue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetChue!")
            return
        back = self.SetRegister(0x59, val_int)
        if self.verbose and back[0] == 1:
            print('Setting CYAN HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting CYAN HUE to ' + str(val_int + ' has Failed!'))

    def GetChue(self):
        back = self.GetRegister(0x59)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('CYAN HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading CYAN HUE has Failed!')

    def SetCgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetCgain!")
            return
        back = self.SetRegister(0x5a, val_int)
        if self.verbose and back[0] == 1:
            print('Setting CYAN GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting CYAN GAIN to ' + str(val_int + ' has Failed!'))

    def GetCgain(self):
        back = self.GetRegister(0x5a)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('CYAN GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading CYAN GAIN has Failed!')

    def SetBhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetBhue!")
            return
        back = self.SetRegister(0x55, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BLUE HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BLUE HUE to ' + str(val_int + ' has Failed!'))

    def GetBhue(self):
        back = self.GetRegister(0x55)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BLUE HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BLUE HUE has Failed!')

    def SetBgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetBgain!")
            return
        back = self.SetRegister(0x56, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BLUE GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BLUE GAIN to ' + str(val_int + ' has Failed!'))

    def GetBgain(self):
        back = self.GetRegister(0x56)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BLUE GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BLUE GAIN has Failed!')

    def SetMhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetMhue!")
            return
        back = self.SetRegister(0x5b, val_int)
        if self.verbose and back[0] == 1:
            print('Setting MAGENTA HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MAGENTA HUE to ' + str(val_int + ' has Failed!'))

    def GetMhue(self):
        back = self.GetRegister(0x5b)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('MAGENTA HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MAGENTA HUE has Failed!')

    def SetMgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetMgain!")
            return
        back = self.SetRegister(0x5c, val_int)
        if self.verbose and back[0] == 1:
            print('Setting MAGENTA GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MAGENTA GAIN to ' + str(val_int + ' has Failed!'))

    def GetMgain(self):
        back = self.GetRegister(0x5c)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('MAGENTA GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MAGENTA GAIN has Failed!')

    def SetRyhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetRyhue!")
            return
        back = self.SetRegister(0xb4, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RED-YELLOW HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RED-YELLOW HUE to ' + str(val_int + ' has Failed!'))

    def GetRyhue(self):
        back = self.GetRegister(0xb4)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RED YELLOW HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RED YELLOW HUE has Failed!')

    def SetRygain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetRygain!")
            return
        back = self.SetRegister(0xb5, val_int)
        if self.verbose and back[0] == 1:
            print('Setting RED-YELLOW GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting RED-YELLOW GAIN to ' + str(val_int + ' has Failed!'))

    def GetRygain(self):
        back = self.GetRegister(0xb5)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('RED-YELLOW GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading RED-YELLOW GAIN has Failed!')

    def SetYghue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetYghue!")
            return
        back = self.SetRegister(0xb6, val_int)
        if self.verbose and back[0] == 1:
            print('Setting YELLOW-GREEN HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting YELLOW-GREEN HUE to ' + str(val_int + ' has Failed!'))

    def GetYghue(self):
        back = self.GetRegister(0xb6)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('YELLOW-GREEN HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading YELLOW-GREEN HUE has Failed!')

    def SetYggain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetYggain!")
            return
        back = self.SetRegister(0xb7, val_int)
        if self.verbose and back[0] == 1:
            print('Setting YELLOW-GREEN GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting YELLOW-GREEN GAIN to ' + str(val_int + ' has Failed!'))

    def GetYggain(self):
        back = self.GetRegister(0xb7)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('YELLOW-GREEN GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading YELLOW-GREEN GAIN has Failed!')

    def SetGchue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetGchue!")
            return
        back = self.SetRegister(0xb8, val_int)
        if self.verbose and back[0] == 1:
            print('Setting GREEN-CYAN HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GREEN-CYAN HUE to ' + str(val_int + ' has Failed!'))

    def GetGchue(self):
        back = self.GetRegister(0xb8)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GREEN-CYAN HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GREEN-CYAN HUE has Failed!')

    def SetGcgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetGcgain!")
            return
        back = self.SetRegister(0xb9, val_int)
        if self.verbose and back[0] == 1:
            print('Setting GREEN-CYAN GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting GREEN-CYAN GAIN to ' + str(val_int + ' has Failed!'))

    def GetGcgain(self):
        back = self.GetRegister(0xb9)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('GREEN-CYAN GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading GREEN-CYAN GAIN has Failed!')

    def SetCbhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetCbhue!")
            return
        back = self.SetRegister(0xba, val_int)
        if self.verbose and back[0] == 1:
            print('Setting CYAN-BLUE HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting CYAN-BLUE HUE to ' + str(val_int + ' has Failed!'))

    def GetCbhue(self):
        back = self.GetRegister(0xba)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('CYAN-BLUE HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading CYAN-BLUE HUE has Failed!')

    def SetCbgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetCbgain!")
            return
        back = self.SetRegister(0xbb, val_int)
        if self.verbose and back[0] == 1:
            print('Setting CYAN-BLUE GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting CYAN-BLUE GAIN to ' + str(val_int + ' has Failed!'))

    def GetCbgain(self):
        back = self.GetRegister(0xbb)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('CYAN-BLUE GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading CYAN-BLUE GAIN has Failed!')

    def SetBmhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetBmhue!")
            return
        back = self.SetRegister(0xbc, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BLUE-MAGENTA HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BLUE-MAGENTA HUE to ' + str(val_int + ' has Failed!'))

    def GetBmhue(self):
        back = self.GetRegister(0xbc)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BLUE-MAGENTA HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BLUE-MAGENTA HUE has Failed!')

    def SetBmgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetBmgain!")
            return
        back = self.SetRegister(0xbd, val_int)
        if self.verbose and back[0] == 1:
            print('Setting BLUE-MAGENTA GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting BLUE-MAGENTA GAIN to ' + str(val_int + ' has Failed!'))

    def GetBmgain(self):
        back = self.GetRegister(0xbd)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BLUE-MAGENTA GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BLUE-MAGENTA GAIN has Failed!')

    def SetMrhue(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetMrhue!")
            return
        back = self.SetRegister(0xbe, val_int)
        if self.verbose and back[0] == 1:
            print('Setting MAGENTA-RED HUE to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MAGENTA-RED HUE to ' + str(val_int + ' has Failed!'))

    def GetMrhue(self):
        back = self.GetRegister(0xbe)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('MAGENTA-RED HUE set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MAGENTA-RED HUE has Failed!')

    def SetMrgain(self, val_int):
        if val_int < -15 or val_int > 15:
            print("Invalid argument to SetMrgain!")
            return
        back = self.SetRegister(0xbf, val_int)
        if self.verbose and back[0] == 1:
            print('Setting MAGENTA-RED GAIN to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MAGENTA-RED GAIN to ' + str(val_int + ' has Failed!'))

    def GetMrgain(self):
        back = self.GetRegister(0xbf)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('MAGENTA-RED GAIN set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MAGENTA-RED GAIN has Failed!')

    def SetChroma(self, val_int):
        if val_int < -31 or val_int > 31:
            print("Invalid argument to SetChroma!")
            return
        back = self.SetRegister(0x47, val_int)
        if self.verbose and back[0] == 1:
            print('Setting CHROMA to ' + str(val_int))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting CHROMA to ' + str(val_int + ' has Failed!'))

    def GetChroma(self):
        back = self.GetRegister(0x47)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('CHROMA set to ' + str(back[1][2]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading CHROMA has Failed!')

    ###########Option######################

    def SetScale(self, val_int):
        scale = {0: '1.0x', 1: '1.5x', 2:'2.0x'}
        if not val_int in scale.keys():
            print("Invalid argument to Set Scale! Please Enter a value 0-2")
            return
        back = self.SetRegister(0x9a, val_int)
        if self.verbose and back[0] == 1:
            print('Setting SCALE to ' + str(scale[val_int]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting SCALE to ' + str(scale[val_int]) + ' has Failed!')

    def GetScale(self):
        scale = {0: '1.0x', 1: '1.5x', 2: '2.0x'}
        back = self.GetRegister(0x9a)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('SCALE set to ' + str(scale[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading SCALE has Failed!')

    def SetDTL(self, val_int):
        dtl = {0: 'OFF', 1: 'ON'}
        if not val_int in dtl.keys():
            print("Invalid argument to Set DTL! Please Enter a value 0-1")
            return
        back = self.SetRegister(0x83, val_int)
        if self.verbose and back[0] == 1:
            print('Setting DTL to ' + str(dtl[val_int]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting DTL to ' + str(dtl[val_int]) + ' has Failed!')

    def GetDTL(self):
        dtl = {0: 'OFF', 1: 'ON'}
        back = self.GetRegister(0x83)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('DTL set to ' + str(dtl[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading DTL has Failed!')

    def SetFreeze(self, val_int):
        freeze = {0: 'OFF', 1: 'ON'}
        if not val_int in freeze.keys():
            print("Invalid argument to Set Freeze! Please Enter a value 0-1")
            return
        back = self.SetRegister(0x87, val_int)
        if self.verbose and back[0] == 1:
            print('Setting FREEZE to ' + str(freeze[val_int]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting FREEZE to ' + str(freeze[val_int]) + ' has Failed!')

    def GetFreeze(self):
        freeze = {0: 'OFF', 1: 'ON'}
        back = self.GetRegister(0x87)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('FREEZE set to ' + str(freeze[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading FREEZE has Failed!')

    def SetBaud(self, val_int):
        if val_int < 0 or val_int > 1:
            print("Invalid argument to Set Freeze! Please Enter a value 0-1")
            return

        back = self.GetRegister(0x88)[0]
        if back == 2:
            self.changeBaud()

        if self.ser.baudrate == 19200 and val_int == 0:
            self.SetRegister(0x88, val_int, baudchange = True)
            if self.verbose:
                print('Changing Baud to 9600 bps')
        elif self.ser.baudrate == 9600 and val_int == 1:
            self.SetRegister(0x88, val_int, baudchange = True)
            if self.verbose:
                print('Changing Baud to 19200 bps')
        elif self.ser.baudrate == 9600 and val_int == 0 and self.verbose:
            print('Baud already set to 9600 bps')
        else:
            print('Baud already set to 19200 bps')

    def GetBaud(self):
        baud = {0: '9600 bps', 1: '19200 bps'}
        back = self.GetRegister(0x88)
        if back[0] == 1:
            return back[1][2]
        elif self.verbose and back[0] == 1:
            print('BAUD set to ' + str(baud[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading BAUD has Failed!')

    def SetMask(self, val_int):
        freeze = {0: 'OFF', 1: 'ON'}
        if not val_int in freeze.keys():
            print("Invalid argument to Set MASK! Please Enter a value 0-1")
            return
        back = self.SetRegister(0x8a, val_int)
        if self.verbose and back[0] == 1:
            print('Setting MASK to ' + str(freeze[val_int]))
        elif self.verbose and back[0] == 0:
            print('FAULT: Setting MASK to ' + str(freeze[val_int]) + ' has Failed!')

    def GetMask(self):
        freeze = {0: 'OFF', 1: 'ON'}
        back = self.GetRegister(0x8a)
        if back[0] == 1:
            return back[1][2]
        if self.verbose and back[0] == 1:
            print('MASK set to ' + str(freeze[back[1][2]]))
            return back[1][2]
        elif self.verbose and back[0] == 0:
            print('FAULT: Reading MASK has Failed!')