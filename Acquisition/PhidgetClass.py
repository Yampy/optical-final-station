# -*- coding: utf-8 -*-
"""
Created on Wed Aug 24 17:52:34 2016

@author: optics.testone
"""

'''
USAGE:

#create the class;
from PhidgetsAnalog import LED
derp = LED()

#pick a channel and set a voltage (0 V is brightest, 3.9-4.1 V is dimmest (varies))
derp.setVoltage(chan,voltage)

#close communication and reset voltages to 0;
derp.close()

'''


from Phidgets.PhidgetException import PhidgetException
from Phidgets.Devices.Analog import Analog
from sys import exit


class LED:
    def __init__(self):
        try:
            self.analog = Analog()
        except RuntimeError as e:
            print("Runtime Exception: %s" % e.details)
            print("Exiting....")
            exit(1)
            
        try:
            self.analog.setOnAttachHandler(self.AnalogAttached)
            self.analog.setOnDetachHandler(self.AnalogDetached)
            self.analog.setOnErrorhandler(self.AnalogError)
            self.analog.openPhidget()
            self.analog.waitForAttach(10000)
            self.enableChannels()            
        
        except PhidgetException as e:
            print("Phidget Exception %i: %s" % (e.code, e.details))
            try:
                self.analog.closePhidget()
            except PhidgetException as e:
                print("Phidget Exception %i: %s" % (e.code, e.details))
                print("Exiting....")
                exit(1)
            print("Exiting....")
            exit(1)

            
    #Event Handler Callback Functions
    def AnalogAttached(self,e):
        attached = e.device
        print("\nPhidget Analog %i Attached! \nLED Controls Enabled... \n" % (attached.getSerialNum()))

    def AnalogDetached(self,e):
        detached = e.device
        print("\nPhidget Analog %i Detached!\n" % (detached.getSerialNum()))

    def AnalogError(self,e):
        try:
            source = e.device
            print("\nPhidget Analog %i: Phidget Error %i: %s\n" % (source.getSerialNum(), e.eCode, e.description))
        except PhidgetException as e:
            print("\nPhidget Exception %i: %s\n" % (e.code, e.details))
    
    def enableChannels(self):
        try:            
            self.analog.setEnabled(0, True)
            self.analog.setEnabled(1, True)
            self.analog.setEnabled(2, True)
            self.analog.setEnabled(3, True)
        except PhidgetException as e:
            print("\nPhidget Exception %i: %s" % (e.code, e.details))
            print("Exiting....\n")
            exit(1)
    
    def setVoltage(self,chan,voltage):
        try:            
            #print("Setting Analog output channel "+str(chan)+" to "+str(voltage)+" V...")
            self.analog.setVoltage(chan, voltage)
        except PhidgetException as e:
            print("Phidget Exception %i: %s" % (e.code, e.details))
            print("Exiting....")
            exit(1)

    def close(self):
        try:
            #5V turns all LED off
            self.analog.setVoltage(0, 5)
            self.analog.setVoltage(1, 5)
            self.analog.setVoltage(2, 5)
            self.analog.setVoltage(3, 5)
            self.analog.setEnabled(0, False)
            self.analog.setEnabled(1, False)
            self.analog.setEnabled(2, False) 
            self.analog.setEnabled(3, False)                  
            self.analog.closePhidget()
        except PhidgetException as e:
            print("Phidget Exception %i: %s" % (e.code, e.details))
            print("Exiting....")
            exit(1)

    def __del__(self):
        self.close()