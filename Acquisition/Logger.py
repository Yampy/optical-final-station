# -*- coding: utf-8 -*-
"""
Created on Thu Dec 22 10:12:01 2016

@author: Yampy
"""
import sys
from time import localtime, strftime

def getDateTime():
    return strftime("%Y-%m-%d_%H-%M-%S", localtime())

class Logger(object):
    errors = sys.stdout.errors  
    encoding = sys.stdout.encoding
    
    def __init__(self,stream,filename):
        self.terminal = stream
        sys.stdout.errors
        self.log = open(filename, "a")

    def write(self, message, init = False):        
        if init:
            self.log.write(message)
        else:
            self.terminal.write(message)
            self.terminal.flush()
            self.log.write(message)

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass

    def close_logger(self):
        self.log.close()
